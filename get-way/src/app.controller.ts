import { Controller, Get,Post,Body,Put,Delete } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  displayAll() {
    return this.appService.displayAll();
  }
  @Post()
  insertStudent(@Body() data): any {
      console.log(data)
      return this.appService.insertStudent(data);
  } 
  @Put()
  editStudent(@Body() data): any {
      return this.appService.editStudent(data);
  } 
  @Delete()
  removeStudent(@Body() data): any {
      return this.appService.removeStudent(data);
  }  
}
