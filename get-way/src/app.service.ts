import { Injectable,Inject } from '@nestjs/common';
import { ClientProxy ,EventPattern} from "@nestjs/microservices";
import { ServiceBroker } from "moleculer";
import {InjectBroker} from 'nestjs-moleculer'
import { map } from "rxjs/operators";

@Injectable()
export class AppService {
  constructor(
    @InjectBroker('actionStudent') private readonly clientServiceA: ServiceBroker,
    
  ) {}
  async displayAll() {
   let result=null;
    await this.clientServiceA.call("actionStudent.displayAll", {}).then((res) => {
      result=res;
    });
    return result;
    // return 'Hello World!';
  }
  async insertStudent(data){
    let result=null;
    
    await this.clientServiceA.call("actionStudent.insertStudent", data).then((res) => {
      result=res;
    });
    if(result==0) return 'خطا در ثبت اطلاعات ممکن است شماره دانشجویی تکراری باشد.'
    else
        return 'اطلاعات با موفقیت ثبت شد.';
  }
  async editStudent(data){
    let result=null;
    await this.clientServiceA.call("actionStudent.editStudent", data).then((res) => {
      result=res;
    });
    if(result==0) 
        return 'خطا در ویرایش اطلاعات.'
    else
        return 'اطلاعات با موفقیت ویرایش شد.';
  }

  async removeStudent(data){
    let result=null;
    await this.clientServiceA.call("actionStudent.removeStudent", data).then((res) => {
      result=res;
    });
    if(result==0) 
    return 'خطا در حذف اطلاعات.'
    else
    return 'اطلاعات با موفقیت حذف شد.';
  }
}


