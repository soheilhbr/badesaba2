import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ClientsModule, Transport } from "@nestjs/microservices";
import {MoleculerModule} from 'nestjs-moleculer'
@Module({
  imports: [     MoleculerModule.forRoot({
    brokerName: "actionStudent", // if you have multiple broker
    // namespace: "actionStudent", // some moleculer options
    transporter: "nats://localhost:4222",
    // hotReload: true, // hotReload feature from moleculer will not work 
})
],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
