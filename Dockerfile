FROM node:14

# Create app directory
WORKDIR /app/
# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY ./microservice/package*.json ./microservice/



COPY ./get-way/package*.json ./get-way/

RUN npm install ./microservice/
RUN npm install ./get-way/

# If you are building your code for production
# RUN npm ci --only=production

# Bundle app source
COPY . .

EXPOSE 8080
CMD [ "node", "./microservice/index.js" ]

# EXPOSE 3000
CMD ["npm", "run", "start:prod","./get-way/"]